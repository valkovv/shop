package valko.victor.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import valko.victor.shop.repository.*;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class ShopApplication {

//    @Autowired
//    private CartRepository cartRepository;
//
//    @Autowired
//    private CategoryRepository categoryRepository;
//
//    @Autowired
//    private SubCategoryRepository subCategoryRepository;
//
//    @Autowired
//    private OrderRepository orderRepository;
//
//    @Autowired
//    private ProductRepository productRepository;
//
//    @Autowired
//    private UserRepository userRepositoty;
//
//
//    @PostConstruct
//    public void post() {
//        Category category = new Category();
//        category.setName("Electronics");
//
//        Category category1 = new Category();
//        category1.setName("Bikes");
//
//        Category category2 = new Category();
//        category2.setName("Watches");
//
//        categoryRepository.save(category);
//        categoryRepository.save(category1);
//        categoryRepository.save(category2);

//        Category category = categoryRepository.findById(1L).orElseThrow(IllegalArgumentException::new);
//
//        SubCategory phones = SubCategory.builder()
//                .name("Phones")
//                .category(category)
//                .build();
//
//        SubCategory tv = SubCategory.builder()
//                .name("TV")
//                .category(category)
//                .build();
//
//        SubCategory photoVideo = SubCategory.builder()
//                .name("Photo&Video")
//                .category(category)
//                .build();
//
//        subCategoryRepository.save(phones);
//        subCategoryRepository.save(tv);
//        subCategoryRepository.save(photoVideo);

//        SubCategory subCategory = subCategoryRepository.findById(1L).orElseThrow(IllegalArgumentException::new);
//
//        Product product = Product.builder()
//                .name("Apple iPhone 6")
//                .price(400)
//                .subCategory(subCategory)
//                .descriptionMin("Экран (4.7, IPS, 1334x750)/ Apple 6 (1.4 ГГц)/ основная камера: 8 Мп, фронтальная камера: 1.2 Мп/ RAM 1 ГБ/ 32 ГБ встроенной памяти/ 3G/ LTE/ GPS/ Nano-SIM/ iOS/ 1810 мА*ч")
//                .descriptionMax("Экран (4.7, IPS, 1334x750)/ Apple 6 (1.4 ГГц)/ основная камера: 8 Мп, фронтальная камера: 1.2 Мп/ RAM 1 ГБ/ 32 ГБ встроенной памяти/ 3G/ LTE/ GPS/ Nano-SIM/ iOS/ 1810 мА*ч")
//                .build();
//
//        Product product1 = Product.builder()
//                .name("Apple iPhone 7")
//                .price(500)
//                .subCategory(subCategory)
//                .descriptionMin("Экран (4.7, IPS, 1334x750)/ Apple 7 (1.4 ГГц)/ основная камера: 8 Мп, фронтальная камера: 1.2 Мп/ RAM 1 ГБ/ 32 ГБ встроенной памяти/ 3G/ LTE/ GPS/ Nano-SIM/ iOS/ 1810 мА*ч")
//                .descriptionMax("Экран (4.7, IPS, 1334x750)/ Apple 7 (1.4 ГГц)/ основная камера: 8 Мп, фронтальная камера: 1.2 Мп/ RAM 1 ГБ/ 32 ГБ встроенной памяти/ 3G/ LTE/ GPS/ Nano-SIM/ iOS/ 1810 мА*ч")
//                .build();
//
//        Product product2 = Product.builder()
//                .name("Apple iPhone 8")
//                .price(600)
//                .subCategory(subCategory)
//                .descriptionMin("Экран (4.7, IPS, 1334x750)/ Apple 8 (1.4 ГГц)/ основная камера: 8 Мп, фронтальная камера: 1.2 Мп/ RAM 1 ГБ/ 32 ГБ встроенной памяти/ 3G/ LTE/ GPS/ Nano-SIM/ iOS/ 1810 мА*ч")
//                .descriptionMax("Экран (4.7, IPS, 1334x750)/ Apple 8 (1.4 ГГц)/ основная камера: 8 Мп, фронтальная камера: 1.2 Мп/ RAM 1 ГБ/ 32 ГБ встроенной памяти/ 3G/ LTE/ GPS/ Nano-SIM/ iOS/ 1810 мА*ч")
//                .build();
//
//        productRepository.save(product);
//        productRepository.save(product1);
//        productRepository.save(product2);


//        User user = new User();
//        user.setLogin("02");
//
//        userRepositoty.save(user);

//        categoryRepository.findAll().forEach(System.out::println);
//    }

    public static void main(String[] args) {
        SpringApplication.run(ShopApplication.class, args);
    }

}
