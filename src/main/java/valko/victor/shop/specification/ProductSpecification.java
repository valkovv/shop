package valko.victor.shop.specification;

import org.springframework.data.jpa.domain.Specification;
import valko.victor.shop.entity.Product;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class ProductSpecification implements Specification<Product> {

    private String name;
    private Integer minPrice;
    private Integer maxPrice;

    public ProductSpecification(String name, Integer minPrice, Integer maxPrice) {
        this.name = name;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(byName(root, criteriaBuilder));
        predicates.add(byPrice(root, criteriaBuilder));

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));

    }

    private Predicate byName(Root<Product> root, CriteriaBuilder criteriaBuilder){
        return criteriaBuilder.like(root.get("name"), "%" + name + "%");
    }

    private Predicate byPrice(Root<Product> root, CriteriaBuilder criteriaBuilder){
        return criteriaBuilder.between(root.get("price"), minPrice, maxPrice);
    }
}
