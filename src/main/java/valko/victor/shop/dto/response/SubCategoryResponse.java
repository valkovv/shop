package valko.victor.shop.dto.response;

import lombok.Getter;
import lombok.Setter;
import valko.victor.shop.entity.SubCategory;

import javax.persistence.Column;

@Getter
@Setter
public class SubCategoryResponse {

    private Long id;

    @Column(unique = true, nullable = false)
    private String name;

    private CategoryResponse categoryResponse;

    public SubCategoryResponse(SubCategory subCategory) {
        id = subCategory.getId();
        name = subCategory.getName();
        categoryResponse = new CategoryResponse(subCategory.getCategory());
    }
}
