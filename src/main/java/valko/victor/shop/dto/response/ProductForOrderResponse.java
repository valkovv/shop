package valko.victor.shop.dto.response;

import lombok.Getter;
import lombok.Setter;
import valko.victor.shop.entity.ProductForOrder;

@Getter
@Setter
public class ProductForOrderResponse {

    private Long id;

    private Integer count;

    private ProductResponse productResponse;

    public ProductForOrderResponse(ProductForOrder productForOrder) {
        id = productForOrder.getId();
        count = productForOrder.getCount();
        productResponse = new ProductResponse(productForOrder.getProduct());
    }
}
