package valko.victor.shop.dto.response;

import lombok.Getter;
import lombok.Setter;
import valko.victor.shop.entity.Product;

@Getter
@Setter
public class ProductResponse {

    private Long id;

    private String name;

    private Integer price;

    private String description;

    private String pathToImg;

    private SubCategoryResponse subCategoryResponse;

    public ProductResponse(Product product) {
        id = product.getId();
        name = product.getName();
        price = product.getPrice();
        description = product.getDescription();
        pathToImg = product.getPathToImg();
        subCategoryResponse = new SubCategoryResponse(product.getSubCategory());
    }
}
