package valko.victor.shop.dto.response;

import lombok.Getter;
import lombok.Setter;
import valko.victor.shop.entity.Category;

@Getter
@Setter
public class CategoryResponse {
    private Long id;

    private String name;

    public CategoryResponse(Category category){
        id = category.getId();
        name = category.getName();
    }
}
