package valko.victor.shop.dto.response;

import lombok.Getter;
import lombok.Setter;
import valko.victor.shop.entity.User;

@Getter
@Setter
public class UserResponse {

    private Long id;

    private String login;

    private String password;

    private String email;

    public UserResponse(User user) {
        id = user.getId();
        login = user.getLogin();
        password = user.getPassword();
        email = user.getEmail();
    }
}
