package valko.victor.shop.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import valko.victor.shop.entity.Order;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class OrderResponse {

    private Long id;
    private UserResponse userResponse;
    private String phoneNumber;
    private String email;
    private String address;
    private String comment;
    private Long sum;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime posted;

    private Boolean done;

    @JsonProperty("products")
    private List<ProductForOrderResponse> productForOrderResponses;

    public OrderResponse(Order order) {
        id = order.getId();
        userResponse = new UserResponse(order.getUser());
        phoneNumber = order.getPhoneNumber();
        email = order.getEmail();
        address = order.getAddress();
        comment = order.getComment();
        posted = order.getPosted();
        done = order.getDone();
        productForOrderResponses = order.getProductForOrders().stream().map(ProductForOrderResponse::new).collect(Collectors.toList());

    }
}
