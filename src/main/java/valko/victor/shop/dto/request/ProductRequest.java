package valko.victor.shop.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@Setter
public class ProductRequest {

    @NotBlank
    @Size(min = 6, max = 30)
    private String name;
    @NotNull
    @Positive
    private Integer price;

    private String description;

    private String pathToImg;
    @NotNull
    private Long subCategoryId;


}


