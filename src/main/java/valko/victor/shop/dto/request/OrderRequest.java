package valko.victor.shop.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class OrderRequest {

    @NotNull
    private String phoneNumber;

    private String email;

    private String address;

    private String comment;

    private Long userId;

    @NotEmpty
    private List<ProductForOrderRequest> products;
}
