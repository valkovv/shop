package valko.victor.shop.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductForOrderRequest {
    @NotNull
    private Integer count;

    @NotNull
    private Long productId;

}
