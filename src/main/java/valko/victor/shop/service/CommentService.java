package valko.victor.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import valko.victor.shop.dto.request.CommentRequest;
import valko.victor.shop.dto.response.CommentResponse;
import valko.victor.shop.entity.Comment;
import valko.victor.shop.exception.WrongInputDataException;
import valko.victor.shop.repository.CommentRepository;


import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private ProductService productService;

    public CommentResponse save(CommentRequest commentRequest) {
        return new CommentResponse (commentRepository.save(commentRequestToComment(null, commentRequest)));
    }

    public List<CommentResponse> findAll() {
        return commentRepository.findAll().stream().map(CommentResponse::new).collect(Collectors.toList());
    }

    public List<CommentResponse> findAllByProductId(Long productId) {
        return commentRepository.findAllByProductId(productId).stream().map(CommentResponse::new).collect(Collectors.toList());
    }

    public List<CommentResponse> findAllToVisibleByProductId(Long productId) {
        return commentRepository.findAllByProductIdAndVisibleIsTrue(productId).stream().map(CommentResponse::new).collect(Collectors.toList());
    }

    public CommentResponse update(Long id, CommentRequest commentRequest) {
        return new CommentResponse (commentRepository.save(commentRequestToComment(findOne(id), commentRequest)));
    }

    public void delete(Long id) {
        commentRepository.delete(findOne(id));
    }

    public Comment findOne(Long id) {
        return commentRepository.findById(id).orElseThrow(() -> new WrongInputDataException("Comment with id " + id + " not exists"));
    }

    private Comment commentRequestToComment(Comment comment, CommentRequest commentRequest) {
        if (comment == null) {
            comment = new Comment();
            comment.setVisible(true);
            comment.setDatePosted(LocalDateTime.now(ZoneId.of("Europe/Kiev")));
        }
        comment.setUserName(commentRequest.getUserName());
        comment.setText(commentRequest.getText());
        comment.setProduct(productService.findOne(commentRequest.getProductId()));
        return comment;
    }
}
