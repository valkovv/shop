package valko.victor.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import valko.victor.shop.dto.request.SubCategoryRequest;
import valko.victor.shop.dto.response.DataResponse;
import valko.victor.shop.dto.response.SubCategoryResponse;
import valko.victor.shop.entity.Category;
import valko.victor.shop.entity.SubCategory;
import valko.victor.shop.exception.WrongInputDataException;
import valko.victor.shop.repository.SubCategoryRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubCategoryService {

    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @Autowired
    private CategoryService categoryService;

    public SubCategoryResponse save(SubCategoryRequest subCategoryRequest) {
        return new SubCategoryResponse(subCategoryRepository.save(subCategoryRequestToSubCategory(subCategoryRequest, null)));
    }

    public List<SubCategoryResponse> findAll() {
        return subCategoryRepository.findAll().stream().map(SubCategoryResponse::new).collect(Collectors.toList());
    }

    public List<SubCategoryResponse> findAllByCategoryId(Long categoryId) {
        return subCategoryRepository.findAllByCategoryId(categoryId).stream().map(SubCategoryResponse::new).collect(Collectors.toList());
    }

    public SubCategoryResponse update(SubCategoryRequest subCategoryRequest, Long id) {
        return new SubCategoryResponse(subCategoryRepository.save(subCategoryRequestToSubCategory(subCategoryRequest, findOne(id))));
    }

    public void delete(Long id) {
        subCategoryRepository.delete(findOne(id));
    }

    public SubCategory findOne(Long id) {
        return subCategoryRepository.findById(id).orElseThrow(() -> new WrongInputDataException("SubCategory with id " + id + " not exists"));
    }

    private SubCategory subCategoryRequestToSubCategory(SubCategoryRequest subCategoryRequest, SubCategory subCategory) {
        if (subCategory == null) {
            subCategory = new SubCategory();
        }
        subCategory.setName(subCategoryRequest.getName());

        Category category = categoryService.findOne(subCategoryRequest.getCategoryId());

        subCategory.setCategory(category);
        return subCategory;
    }
}

