package valko.victor.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import valko.victor.shop.dto.request.ProductRequest;
import valko.victor.shop.dto.response.DataResponse;
import valko.victor.shop.dto.response.ProductResponse;
import valko.victor.shop.entity.Product;
import valko.victor.shop.entity.SubCategory;
import valko.victor.shop.exception.WrongInputDataException;
import valko.victor.shop.repository.ProductRepository;
import valko.victor.shop.specification.ProductSpecification;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private FileService fileService;

    public ProductResponse save(ProductRequest productRequest) throws WrongInputDataException, IOException {
        return new ProductResponse(productRepository.save(productRequestToProduct(productRequest, null)));
    }

    public List<ProductResponse> findAll() {
        return productRepository.findAll().stream().map(ProductResponse::new).collect(Collectors.toList());
    }

    public DataResponse<ProductResponse> findPage(Integer page, Integer size, String sortFieldName, Sort.Direction direction) {
        PageRequest pageRequest = PageRequest.of(page, size, direction, sortFieldName);
        Page<Product> pageProduct = productRepository.findAll(pageRequest);
        List<ProductResponse> collect = pageProduct.get().map(ProductResponse::new).collect(Collectors.toList());
        return new DataResponse<>(pageProduct.getTotalPages(), pageProduct.getTotalElements(), collect);
    }

    public ProductResponse update(ProductRequest productRequest, Long id) throws WrongInputDataException, IOException {
        return new ProductResponse(productRepository.save(productRequestToProduct(productRequest, findOne(id))));
    }

    public void delete(Long id) {
        productRepository.delete(findOne(id));
    }

    public Product findOne(Long id) {
        return productRepository.findById(id).orElseThrow(() -> new WrongInputDataException("Product with id " + id + " not exists"));
    }

    private Product productRequestToProduct(ProductRequest productRequest, Product product) throws IOException {
        if (product == null) {
            product = new Product();
        }
        product.setName(productRequest.getName());
        product.setPrice(productRequest.getPrice());
        product.setDescription(productRequest.getDescription());

        SubCategory subCategory = subCategoryService.findOne(productRequest.getSubCategoryId());
        product.setSubCategory(subCategory);
        if (productRequest.getPathToImg() != null && !productRequest.getPathToImg().isEmpty()) {
            product.setPathToImg(fileService.saveFile(productRequest.getPathToImg()));
        }
        return product;
    }

    public DataResponse<ProductResponse> getPageByFilter(Integer minPrice, Integer maxPrice, String name, Integer page, Integer size, String fieldName, Sort.Direction direction) {
        Page<Product> all = productRepository.findAll(new ProductSpecification(name, minPrice, maxPrice),
                PageRequest.of(page, size, direction, fieldName));
        List<ProductResponse> collect = all.get().map(ProductResponse::new).collect(Collectors.toList());
        return new DataResponse<>(all.getTotalPages(), all.getTotalElements(), collect);
    }
}
