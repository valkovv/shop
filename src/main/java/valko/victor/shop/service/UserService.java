package valko.victor.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import valko.victor.shop.dto.request.UserRequest;
import valko.victor.shop.dto.response.UserResponse;
import valko.victor.shop.entity.User;
import valko.victor.shop.exception.WrongInputDataException;
import valko.victor.shop.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserResponse save(UserRequest userRequest) {
        return new UserResponse(userRepository.save(userRequestToUser(userRequest, null)));
    }

    public List<UserResponse> findAll() {
        return userRepository.findAll().stream().map(UserResponse::new).collect(Collectors.toList());
    }

    public UserResponse update(UserRequest userRequest, Long id) {
        return new UserResponse(userRepository.save(userRequestToUser(userRequest, findOne(id))));
    }

    public void delete(Long id) {
        userRepository.delete(findOne(id));
    }

    public User findOne(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new WrongInputDataException("User with id " + id + " not exists"));
    }

    private User userRequestToUser(UserRequest userRequest, User user) {
        if (user == null) {
            user = new User();
        }
        user.setLogin(userRequest.getLogin());
        user.setPassword(userRequest.getPassword());
        user.setEmail(userRequest.getEmail());


        return user;
    }
}
