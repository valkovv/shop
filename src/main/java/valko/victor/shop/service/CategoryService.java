package valko.victor.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import valko.victor.shop.dto.request.CategoryRequest;
import valko.victor.shop.dto.response.CategoryResponse;
import valko.victor.shop.entity.Category;
import valko.victor.shop.exception.WrongInputDataException;
import valko.victor.shop.repository.CategoryRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public CategoryResponse save(CategoryRequest categoryRequest) {
        Category category = new Category();
        category.setName(categoryRequest.getName());
        Category save = categoryRepository.save(category);
        return new CategoryResponse(save);
    }

    public List<CategoryResponse> findAll() {
        return categoryRepository.findAll()
                .stream()
                .map(CategoryResponse::new)
                .collect(Collectors.toList());
    }

    public CategoryResponse update(CategoryRequest categoryRequest, Long id) {
        Category category = findOne(id);
        category.setName(categoryRequest.getName());
        Category save = categoryRepository.save(category);
        return new CategoryResponse(save);
    }

    public void delete(Long id) {
        categoryRepository.delete(findOne(id));
    }

    public Category findOne(Long id) {
        return categoryRepository.findById(id).orElseThrow(() -> new WrongInputDataException("Category with id " + id + " not exists"));
    }
}

