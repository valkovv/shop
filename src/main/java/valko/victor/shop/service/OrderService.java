package valko.victor.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import valko.victor.shop.dto.request.OrderRequest;
import valko.victor.shop.dto.request.ProductForOrderRequest;
import valko.victor.shop.dto.response.OrderResponse;
import valko.victor.shop.entity.Order;
import valko.victor.shop.entity.ProductForOrder;
import valko.victor.shop.entity.User;
import valko.victor.shop.exception.WrongInputDataException;
import valko.victor.shop.repository.OrderRepository;
import valko.victor.shop.repository.ProductForOrderRepository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductForOrderRepository productForOrderRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    public OrderResponse save(OrderRequest orderRequest) {
        Order order = orderRepository.save(orderRequestToOrder(null, orderRequest));
        return new OrderResponse(saveProductsForOrder(order, orderRequest));//(orderRepository.save(orderRequestToOrder(null, orderRequest)));
    }

    public List<OrderResponse> findAll() {
        return orderRepository.findAll().stream().map(OrderResponse::new).collect(Collectors.toList());
    }

    public OrderResponse update(OrderRequest orderRequest, Long id) {
        return new OrderResponse(orderRepository.save(orderRequestToOrder(findOne(id), orderRequest)));
    }

    public void delete(Long id) {
        orderRepository.delete(findOne(id));
    }

    public Order findOne(Long id) {
        return orderRepository.findById(id).orElseThrow(() -> new WrongInputDataException("Order with id " + id + " not exists"));
    }

    private Order orderRequestToOrder(Order order, OrderRequest orderRequest) {
        if (order == null) {
            order = new Order();
            order.setPosted(LocalDateTime.now(ZoneId.of("Europe/Kiev")));
        }
        order.setEmail(orderRequest.getEmail());
        order.setAddress(orderRequest.getAddress());
        order.setComment(orderRequest.getComment());

        User user = userService.findOne(orderRequest.getUserId());
        order.setUser(user);
        return order;
    }

    private Order saveProductsForOrder(Order order, OrderRequest request) {
        productForOrderRepository.deleteAll(order.getProductForOrders());
        List<ProductForOrder> productsForOrder = request.getProducts().stream().map(p -> productForOrderRequestToProductForOrder(order, p)).collect(Collectors.toList());
        productForOrderRepository.saveAll(productsForOrder);
        order.setProductForOrders(productsForOrder);
        return order;
    }

    private ProductForOrder productForOrderRequestToProductForOrder(Order order, ProductForOrderRequest request) {
        return ProductForOrder.builder()
                .count(request.getCount())
                .order(order)
                .product(productService.findOne(request.getProductId()))
                .build();
    }


}
