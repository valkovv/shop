package valko.victor.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import valko.victor.shop.dto.request.SubCategoryRequest;
import valko.victor.shop.dto.response.CategoryResponse;
import valko.victor.shop.dto.response.DataResponse;
import valko.victor.shop.dto.response.SubCategoryResponse;
import valko.victor.shop.service.SubCategoryService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/subcategory")
public class SubCategoryController {

    @Autowired
    private SubCategoryService subCategoryService;

    @PostMapping
    public SubCategoryResponse save(@Valid @RequestBody SubCategoryRequest subCategoryRequest) {
        return subCategoryService.save(subCategoryRequest);
    }

    @GetMapping
    public List<SubCategoryResponse> findAll() {
        return subCategoryService.findAll();
    }

    @GetMapping("/bycategoryid")
    public List<SubCategoryResponse> findAllByCategoryId(Long categoryId) {
        return subCategoryService.findAllByCategoryId(categoryId);
    }

    @PutMapping
    public SubCategoryResponse update(@RequestParam Long id, @Valid @RequestBody SubCategoryRequest subCategoryRequest) {
        return subCategoryService.update(subCategoryRequest, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) {
        subCategoryService.delete(id);
    }
}
