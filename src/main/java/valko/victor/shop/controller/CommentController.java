package valko.victor.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import valko.victor.shop.dto.request.CommentRequest;
import valko.victor.shop.dto.response.CommentResponse;
import valko.victor.shop.service.CommentService;

import javax.validation.Valid;
import java.util.List;



@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping
    public CommentResponse save(@Valid @RequestBody CommentRequest request) {
        return commentService.save(request);
    }

    @GetMapping()
    public List<CommentResponse> findAll() {
        return commentService.findAll();
    }

    @GetMapping("/byProduct")
    public List<CommentResponse> findAllByProductId(Long productId) {
        return commentService.findAllByProductId(productId);
    }

    @GetMapping("/visibleByProduct")
    public List<CommentResponse> findAllToVisibleByProductId(Long productId) {
        return commentService.findAllToVisibleByProductId(productId);
    }

    @PutMapping
    public CommentResponse update(Long id, @Valid @RequestBody CommentRequest request) {
        return commentService.update(id, request);
    }

    @DeleteMapping
    public void delete(Long id) {
        commentService.delete(id);
    }
}
