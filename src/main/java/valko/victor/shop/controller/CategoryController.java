package valko.victor.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import valko.victor.shop.dto.request.CategoryRequest;
import valko.victor.shop.dto.response.CategoryResponse;
import valko.victor.shop.service.CategoryService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping
    public CategoryResponse save(@Valid @RequestBody CategoryRequest categoryRequest) {
        return categoryService.save(categoryRequest);
    }

    @GetMapping
    public List<CategoryResponse> findAll() {
        return categoryService.findAll();
    }

    @PutMapping
    public CategoryResponse update(@RequestParam Long id, @Valid @RequestBody CategoryRequest categoryRequest) {
        return categoryService.update(categoryRequest, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) {
        categoryService.delete(id);
    }
}
