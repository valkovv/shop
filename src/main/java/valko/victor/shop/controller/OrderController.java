package valko.victor.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import valko.victor.shop.dto.request.OrderRequest;
import valko.victor.shop.dto.response.OrderResponse;
import valko.victor.shop.service.OrderService;

import java.util.List;


@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping
    public void save(@RequestBody OrderRequest orderRequest) {
        orderService.save(orderRequest);
    }

    @GetMapping
    public List<OrderResponse> findAll() {
        return orderService.findAll();
    }


    @PutMapping
    public OrderResponse update(@RequestParam Long id, @RequestBody OrderRequest orderRequest) {
        return orderService.update(orderRequest, id);
    }

    @DeleteMapping()
    public void delete(@RequestParam Long id) {
        orderService.delete(id);
    }
}
