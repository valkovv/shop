package valko.victor.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import valko.victor.shop.dto.request.UserRequest;
import valko.victor.shop.dto.response.UserResponse;
import valko.victor.shop.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public UserResponse save(@Valid @RequestBody UserRequest userRequest) {
        return userService.save(userRequest);
    }

    @GetMapping
    public List<UserResponse> findAll() {
        return userService.findAll();
    }

    @PutMapping
    public UserResponse update(@RequestParam Long id, @Valid @RequestBody UserRequest userRequest) {
        return userService.update(userRequest, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) {
        userService.delete(id);
    }
}
