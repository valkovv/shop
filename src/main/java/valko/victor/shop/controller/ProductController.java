package valko.victor.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import valko.victor.shop.dto.request.ProductRequest;
import valko.victor.shop.dto.response.DataResponse;
import valko.victor.shop.dto.response.ProductResponse;
import valko.victor.shop.exception.WrongInputDataException;
import valko.victor.shop.service.ProductService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping
    public ProductResponse save(@Valid @RequestBody ProductRequest productRequest) throws WrongInputDataException, IOException {
        return productService.save(productRequest);
    }

    @GetMapping("/page")
    public DataResponse<ProductResponse> findPage(@RequestParam Integer page,
                                                  @RequestParam Integer size,
                                                  @RequestParam String sortFieldName,
                                                  @RequestParam Sort.Direction direction) {
        return productService.findPage(page, size, sortFieldName, direction);
    }

    @GetMapping("/filter")
    public DataResponse<ProductResponse> filter(@RequestParam(required = false) String name,
                                                @RequestParam(required = false) Integer minPrice,
                                                @RequestParam(required = false) Integer maxPrice,
                                                @RequestParam Integer page,
                                                @RequestParam Integer size,
                                                @RequestParam String fieldName,
                                                @RequestParam Sort.Direction direction) {
        return productService.getPageByFilter(minPrice, maxPrice, name, page, size, fieldName, direction);
    }

    @GetMapping
    public List<ProductResponse> findAll() {
        return productService.findAll();
    }

    @PutMapping
    public ProductResponse update(@RequestParam Long id, @Valid @RequestBody ProductRequest productRequest) throws WrongInputDataException, IOException {
        return productService.update(productRequest, id);
    }

    @DeleteMapping()
    public void delete(@RequestParam Long id) {
        productService.delete(id);
    }
}
