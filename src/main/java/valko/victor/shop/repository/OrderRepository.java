package valko.victor.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import valko.victor.shop.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
