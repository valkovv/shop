package valko.victor.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import valko.victor.shop.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
