package valko.victor.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import valko.victor.shop.entity.ProductForOrder;

@Repository
public interface ProductForOrderRepository extends JpaRepository<ProductForOrder, Long> {
}
